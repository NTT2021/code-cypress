class SubmitPage{
    getFirstName(){
        return cy.xpath("//input[@id='firstName']")
    }
    getLastName(){
        return cy.xpath("//input[@id='lastName']")
    }
    getEmail(){
        return cy.xpath("//input[@id='userEmail']")
    }
    getGender(){
        return cy.xpath(`//input[@id='gender-radio-2' and @value = 'Female']`)
    }
    getPhoneNumber(){
        return cy.xpath("//input[@id='userNumber']")
    }
    getDateOfBirth(){
        return cy.xpath("//input[@id='dateOfBirthInput']")
    }
    getYearOfBirth(){
        return cy.xpath("//select[@class='react-datepicker__year-select']")
    }
    getMonthOfBirth(){
        return cy.xpath("//select[@class='react-datepicker__month-select']")
    }
    getDayOfBirth(date){
        return cy.xpath("//div[@role='listbox']/div[1]/div[1]")
    }
    getSubject(){
        return cy.xpath("//div[@class='subjects-auto-complete__control css-yk16xz-control']")
    }
    getHobbi(){
        return cy.xpath("//input[@id='hobbies-checkbox-3']")
    }
    getAddess(){
        return cy.xpath("//textarea[@id='currentAddress']")
    }
    getState(){
        return cy.xpath("//div[@id='state']")
    }
    getCity(){
        return cy.xpath("//div[@id='city']")
    }
    getSubmit(){
        return cy.xpath("//button[@id='submit']")
    }

}
export default SubmitPage