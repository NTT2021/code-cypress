import registerLoginPage from "./registerLoginPage"

class LoginPage{
    getLoginButton(){
        return cy.xpath("//a[@class='ico-login']")
    }
    getEmail(){
        return cy.xpath("//input[@id='Email']")
    }
    getPassword(){
        return cy.xpath("//input[@id='Password']")
    }
    getLogin(){
        return cy.xpath("//button[@class='button-1 login-button']")
    }
    getVerifyLoginSuccessful(){
        return cy.xpath("//div[@class='page-title']/h1")
    }
}
export default LoginPage