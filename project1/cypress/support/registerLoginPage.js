class registerLoginPage{
    getGenderFemale(){
        return cy.xpath("//input[@id='gender-female']")
    }
    getFirstName(){
        return cy.xpath("//input[@id='FirstName']")
    }
    getLastName(){
        return cy.xpath("//input[@id='LastName']")
    }
    getDayOfBirth(){
        return cy.xpath("//select[@name='DateOfBirthDay']")
    }
    getMonthOfBirth(){
        return cy.xpath("//select[@name='DateOfBirthMonth']")
    }
    getYearOfBirth(){
        return cy.xpath("//select[@name='DateOfBirthYear']")
    }
    getEmail(){
        return cy.xpath("//input[@id='Email']")
    }
    getCompanyName(){
        return cy.xpath("//input[@id='Company']")
    }
    
    getPassword(){
        return cy.xpath("//input[@id='Password']")
    }
    getConfirmPassword(){
        return cy.xpath("//input[@id='ConfirmPassword']")
    }
    getRegisterButton(){
        return cy.xpath("//button[@id='register-button']")
    }
    getContinueButton() {
        return cy.get('.button-1.register-continue-button');
    }
}
export default registerLoginPage