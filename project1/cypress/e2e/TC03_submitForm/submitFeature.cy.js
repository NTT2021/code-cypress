import SubmitPage from "../../support/submitPage";
const submitP = new SubmitPage()
Cypress.on('uncaught:exception', (err, runnable) => {
    return false
})
describe('submit form', () => {
    
    it('submit ',()=>{
        cy.viewport(1920,1080)
        cy.visit("https://demoqa.com/automation-practice-form")
        cy.wait(4000)
        submitP.getFirstName().type('A')
        submitP.getLastName().type('B')
        submitP.getEmail().type('abc@gmail.com')
        submitP.getGender().click({force:true})
        submitP.getPhoneNumber().type('0123456789')
        submitP.getDateOfBirth().click()
        submitP.getYearOfBirth().select('2021',{force:true})
        submitP.getMonthOfBirth().select('June',{force:true})
        submitP.getDayOfBirth(20).click()
        submitP.getSubject().type('English{enter}')
        submitP.getHobbi().check({force:true})
        submitP.getAddess().type('Viet Name')
        submitP.getState().type('NCR{enter}')
        submitP.getCity().type('Delhi{enter}')
        submitP.getSubmit().click()
    })
})