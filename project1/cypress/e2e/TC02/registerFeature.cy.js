import registerLoginPage from "../../support/registerLoginPage";
import LoginPage from "../../support/loginPage";
import { describe } from "mocha";
const rLP =new registerLoginPage()
const loginPage = new LoginPage()

describe('demo.nopcommerce', () => {
    //register account
    it('register',()=>{
        cy.visit("https://demo.nopcommerce.com/login?returnUrl=%2F")
        rLP.getGenderFemale().click()
        rLP.getFirstName().type("AA")
        rLP.getLastName().type("BB")
        rLP.getDayOfBirth().select("1")
        rLP.getMonthOfBirth().select("June")
        rLP.getYearOfBirth().select("1912")
        rLP.getEmail().type("abc@gmail.com")
        rLP.getCompanyName().type("ABC Company")
        rLP.getPassword().type("12345678")
        rLP.getConfirmPassword().type("12345678")
        rLP.getRegisterButton().click()
        rLP.getContinueButton().click()
    })

    //login & verify
    it('login&verify',()=>{
        cy.visit("https://demo.nopcommerce.com/login?returnUrl=%2F")
        loginPage.getEmail().type("abc@gmail.com")
        loginPage.getPassword().type("12345678")
        loginPage.getLogin.click()
        loginPage.getVerifyLoginSuccessful.should("contain","Shopping cart")

    }
    )
    })